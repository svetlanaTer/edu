import { createRouter, createWebHistory } from 'vue-router'
import ItemsList from '../components/ItemsList.vue'
import Cart from '../components/Cart.vue'
import NewItem from "@/components/NewItem.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: ItemsList
    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart
    },
    {
      path: '/newitem',
      name: 'newitem',
      component: NewItem
    }
  ]
})

export default router
