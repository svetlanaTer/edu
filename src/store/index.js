import {defineStore} from "pinia";
import {reactive, ref} from "vue";

export const useStore = defineStore('store', {
    state: () => ({
        items: ref([]),
        isLoading: ref('on'),
        cartState: reactive({
            inCart: {}
        }),
        cities : [
            'Москва',
            'Санкт-Петербург',
            'Нижний Новгород',
            'Казань',
            'Тюмень',
            'Ухта',
            'лучшее место на Земле - Ижма'
        ],

    }),
    getters: {
        getFiltered: state => query => {
            return state.items.value.filter(
                (item) => item.title.toLowerCase().indexOf(query.toLowerCase()) !== -1
                    || item.price == query
            )
        },
        getItemCountInCartById: state => id => {
            return state.cartState.inCart[id] ?? 0
        },
        findItem: state => id => {
            return state.items.value.find(i => +i.id === +id)
        },
        getIdsInCart: state => {
            return Object.keys(state.cartState.inCart)
        },
        totalSum: state => {
            let sum = 0;
            let item
            Object.entries(state.cartState.inCart).forEach((el) => {
                item = state.items.value.find(i => +i.id === +el[0])
                console.log(item)
                sum = sum + item.price * el[1].count
            })
            return sum
        },

    },

    actions: {
        async loadCatalog() {
            new Promise((resolve, reject) => {
                return fetch('https://fakestoreapi.com/products')
                    .then(response => {
                        if (response.ok) {
                            resolve(response.json())
                        } else {
                            reject(new Error('error'))
                        }
                    }, error => {
                        reject(new Error(error.message))
                    })
            }).then((vals) => {
                this.items.value = vals;
                this.isLoading = ''
            });
        },

        add2cart(id, count) {
            const item = this.cartState.inCart[id]
            if (!item) {
                const findedItem = this.findItem(id)
                this.cartState.inCart[id] = {
                    count: count,
                    title: findedItem.title ?? ''
                }
            } else {
                this.cartState.inCart[id].count += count
            }
        }
    },

})
